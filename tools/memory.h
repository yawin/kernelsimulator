#ifndef MEMORY_H
#define MEMORY_H

#include "pgb.h"
#include "semaforos.h"
#include "structs.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <string.h>

int RAM_SIZE = 16;
unsigned int PAGESIZE = 256;
int SHOWMEMORY = 0;

Memory RAM;
KernelData kData;

void printBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--) {
        for (j = 7; j >= 0; j--) {
            byte = (b[i] >> j) & 1;
            printf("%u", byte);
        }
    }
    puts("");
}
int checkBits(size_t const size, void const * const ptr)
{
    unsigned char *b = (unsigned char*) ptr;
    unsigned char byte;
    int i, j;

    for (i = size-1; i >= 0; i--)
    {
        for (j = 7; j >= 0; j--)
        {
            byte = (b[i] >> j) & 1;
            if(!byte)
            {
              return j;
            }
        }
    }
    return -1;
}

void InitRam()
{
  RAM.size = 2 << RAM_SIZE;
  kData.pageAmount = RAM.size / PAGESIZE;
  kData.reserved = kData.pageAmount / 8;
  if(kData.pageAmount % 8 > 0)
  {
    kData.reserved++;
  }

  RAM.data = malloc(sizeof(Palabra) * (RAM.size+ kData.reserved));
  memset(RAM.data, 0x00, RAM.size + kData.reserved);

  printf("> Se inicializa la RAM con %d posiciones (%d páginas)\n", RAM.size, kData.pageAmount);
}

int Alloc()
{
  int cB = -1;
  int dF = -1;

  SemaforoDown(&pagetableAccess);
  for(int i = 0; i < kData.reserved && dF == -1; i++)
  {
      cB = checkBits(sizeof(uint8_t), &RAM.data[RAM.size + i]);
      if(cB > -1)
      {
        dF = 8*i+cB;
        uint8_t suma = 0x01 << cB;
        RAM.data[RAM.size + i] = RAM.data[RAM.size + i] | suma;
      }
  }

  if(SHOWMEMORY)
  {
    printf("[MEMORY] <Alloc>\n");
    for(int i = 0; i < kData.reserved; i++)
    {
      printBits(sizeof(uint8_t), &RAM.data[RAM.size + i]);
    }
    printf("\n");
  }

  SemaforoUp(&pagetableAccess);
  return (unsigned int) dF * PAGESIZE;
}

void Free(int dF)
{
  int df = dF / PAGESIZE;
  int cB = df%8;
  int i = (df-cB)/8;

  SemaforoDown(&pagetableAccess);
  uint8_t slot = RAM.data[RAM.size + i];
  uint8_t resta = 0x01 << cB;
  if(slot != 0)
  {
    RAM.data[RAM.size + i] = slot ^ resta;
  }
  if(SHOWMEMORY)
  {
    printf("[MEMORY] <Free>\n");
    for(int i = 0; i < kData.reserved; i++)
    {
      printBits(sizeof(uint8_t), &RAM.data[RAM.size + i]);
    }
    printf("\n");
  }
  SemaforoUp(&pagetableAccess);
}

uint8_t Get(int addr, PGB *pgb)
{
  uint8_t ret;
  SemaforoDown(&ramAccess);
  ret = RAM.data[PGBGet(pgb, addr)];
  SemaforoUp(&ramAccess);
  return ret;
}

unsigned int GetWord(int addr, PGB *pgb)
{
  unsigned int word = 0;
  for(int i = 0; i < 4; i++)
  {
    word <<= 8;
    word += Get(addr+i, pgb);
  }
  return word;
}

void Set(int addr, uint8_t val, PGB *pgb)
{
  SemaforoDown(&ramAccess);
  RAM.data[PGBGet(pgb, addr)] = val;
  SemaforoUp(&ramAccess);
}

void SetWord(int addr, int val, PGB *pgb)
{
  int value = val;
  int v = val;

  for(int i = 3; i >= 0; i--)
  {
    v = (value >> 8*i);
    value = value - (v << 8*i);
    RAM.data[PGBGet(pgb, addr+(3-i))] = v;
  }
}



#endif //MEMORY_H
