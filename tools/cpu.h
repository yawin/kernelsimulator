#ifndef CPU_H
#define CPU_H

/****************************************
  Todo lo relacionado con la CPU
****************************************/

#include "structs.h"
#include "instructionset.h"

#include <stdlib.h>
#include <stdio.h>

int MAX_THREADS = 4;
int MAX_CORES = 4;
int MAX_CPUS = 4;

CPU *cpus;

void InitCPUs()
{
    cpus = malloc(sizeof(CPU) * MAX_CPUS);
    for(unsigned int i = 0; i < MAX_CPUS; i++)
    {
      cpus[i].thread_in_core = 0;
      cpus[i].cores = malloc(sizeof(Core) * MAX_CORES);
      for(unsigned int j = 0; j < MAX_CORES; j++)
      {
        cpus[i].cores[j] = malloc(sizeof(CPU_Task) * MAX_THREADS);
        for(unsigned int k = 0; k < MAX_THREADS; k++)
        {
          cpus[i].cores[j][k] = 0;
        }
      }
    }

    printf("> Se inicializan %d CPUs con %d cores de %d threads\n", MAX_CPUS, MAX_CORES, MAX_THREADS);
}

void CPU_ClockStep()
{
  int h = 0;
  for(unsigned int k = 0; k < MAX_CPUS; k++)
  {
    for(unsigned int i = 0; i < MAX_CORES; i++)
    {
      for(unsigned int j = 0; j < MAX_THREADS; j++)
      {
        if(cpus[k].cores[i][j] != 0)
        {
          if(cpus[k].cores[i][j]->state == S_WORKING)
          {
            if(SHOWINSTRUCTIONS && h++ == 0) printf("[CPU]\n");

            cpus[k].cores[i][j]->quantum--;
            //cpus[k].cores[i][j]->task.life--; //Exit se encarga de poner life a 0
            Execute(cpus[k].cores[i][j]);
            if(cpus[k].cores[i][j]->task.life < 1)
            {
              cpus[k].cores[i][j]->state = S_STOPPED;
            }
          }
        }
      }
    }
  }
  if(SHOWINSTRUCTIONS && h > 0) printf("\n");
}

int CPU_HasSlot(CPU *cpu)
{
  return (cpu->thread_in_core < MAX_THREADS*MAX_CORES);
}

void CPU_Insert(CPU *cpu, CPU_Task *task)
{
  for(unsigned int k = 0; k < MAX_CPUS; k++)
  {
    for(unsigned int i = 0; i < MAX_CORES; i++)
    {
      for(unsigned int j = 0; j < MAX_THREADS; j++)
      {
        if(cpus[k].cores[i][j] == 0)
        {
          cpus[k].cores[i][j] = task;
          cpu->thread_in_core++;
          return;
        }
      }
    }
  }
}

CPU_Task_List CPU_Schedule(CPU *cpu)
{
  CPU_Task_List ctl;

  ctl.data = malloc(sizeof(CPU_Task) * MAX_CORES * MAX_THREADS);
  ctl.size = 0;

  for(unsigned int i = 0; i < MAX_CORES; i++)
  {
    for(unsigned int j = 0; j < MAX_THREADS; j++)
    {
      if(cpu->cores[i][j] != 0)
      {
        if(cpu->cores[i][j]->quantum < 1 || cpu->cores[i][j]->state == S_STOPPED)
        {
          ctl.data[ctl.size] = (*cpu->cores[i][j]);
          ctl.size++;

          cpu->cores[i][j] = 0;
          cpu->thread_in_core = (cpu->thread_in_core - 1 >= 0) ? cpu->thread_in_core-1 : 0;
        }
      }
    }
  }

  return ctl;
}
#endif //CPU_H
