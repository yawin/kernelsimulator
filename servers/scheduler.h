#ifndef SCHEDULER_H
#define SCHEDULER_H

#include "../tools/cpu.h"
#include "../tools/queue.h"
#include "../tools/priorityQueue.h"
#include "../tools/structs.h"

#include "timer.h"

#include <unistd.h>
#include <pthread.h>

extern sem_t oQueueAccess;
extern sem_t cpuAccess;
extern int ktime;
extern int MAX_CPUS;
extern CPU *cpus;

pthread_t dispatcher;
int threads_finished = 0;
double max_time = 0.0, min_time = 999999999999.0;
int SHOWSCHEDULER = 0;

void* Dispatcher()
{
  Task t, nt;

  while(1)
  {
    if(!queueIsEmpty())
    {
      SemaforoDown(&queueAccess);
      t = queueRemoveElement();
      SemaforoUp(&queueAccess);

      if(SHOWSCHEDULER) printf("[Scheduler] <%ld> entra en lista de espera {%d, %d}\n", t.pid, t.life, t.priority);

      Context context;
      context.pc = t.mm.code;

      SemaforoDown(&oQueueAccess);
      priorityQueueInsert(t, context, t.priority);
      SemaforoUp(&oQueueAccess);
    }
  }
}

int cpu_id = 0;
void Scheduler()
{
  CPU_Task_List ctl;
  CPU_Task cput;
  struct timeval  endedAt;
  double tlived;

  int i;
  for(i = 0; i < MAX_CPUS; i++)
  {
    SemaforoDown(&cpuAccess);
    ctl = CPU_Schedule(&cpus[i]);
    SemaforoUp(&cpuAccess);

    for(; ctl.size > 0; ctl.size--)
    {
      cput = ctl.data[ctl.size-1];
      if(cput.state != S_STOPPED && cput.task.life > 0)
      {
        if(SHOWSCHEDULER) printf("[Scheduler] <%ld> vuelve a la lista de espera con nice %d\n", cput.task.pid, cput.quantum);

        SemaforoDown(&oQueueAccess);
        priorityQueueInsert(cput.task, cput.context, cput.task.priority + cput.quantum);
        SemaforoUp(&oQueueAccess);
      }
      else
      {
        PGBErase(&cput.task.mm.pgb);

        gettimeofday (&endedAt, 0);
        tlived = (endedAt.tv_sec - cput.task.startedAt.tv_sec) + (endedAt.tv_usec - cput.task.startedAt.tv_usec)/1e6;
        if(max_time < tlived) max_time = tlived;
        if(min_time > tlived) min_time = tlived;

        if(SHOWSCHEDULER) printf("[Scheduler] <%ld> ha terminado a los %f segundos de crearse\n", cput.task.pid, tlived);
        threads_finished++;
      }
    }

    free(ctl.data);
  }

  if(!priorityQueueIsEmpty())
  {
    priorityQueueNormaliceNices();
  }

  int cont = 0;
  Task t;
  int nice, q;
  CPU_Task *cput2;
  Context context;
  while(!priorityQueueIsEmpty() && cont < MAX_CPUS)
  {
    if(CPU_HasSlot(cpus+cpu_id))
    {
      cont = 0;
      SemaforoDown(&oQueueAccess);
      t = priorityQueueDeleteFirst(&nice, &context);
      SemaforoUp(&oQueueAccess);

      q = nice * 1000;
      if(q > t.life) q = t.life;

      cput2 = malloc(sizeof(CPU_Task));
      cput2->task = t;
      cput2->context = context;
      cput2->quantum = q;
      cput2->state = S_WORKING;

      if(SHOWSCHEDULER) printf("[Scheduler] <%ld> entra en CPU [%d] con quantum %d\n", cput2->task.pid, cpu_id, cput2->quantum);

      SemaforoDown(&cpuAccess);
      CPU_Insert(cpus + cpu_id, cput2);
      SemaforoUp(&cpuAccess);
    }
    else
    {
      cont++;
    }

    cpu_id = (cpu_id + 1 < MAX_CPUS) ? cpu_id + 1 : 0;
  }
}

void* ScheduleServer()
{
  pthread_create(&dispatcher, NULL, Dispatcher, NULL);

  while(1)
  {
    if(ktime >= 1)
    {
      ktime = 0;
      Scheduler();
    }
  }
}
#endif //SCHEDULER_H
